// elemek összegyűjtése

let listItemList = document.getElementById('itemList');
let inputItemName = document.getElementById('itemName');
let buttonAdd = document.getElementById('buttonAdd');
let selectList = document.getElementById('selectList');
let indexNumber = document.getElementById('indexNumber');

let itemList = [

 '1',
 '2',
 '3',
 '4',
 '5',
 '6',
 '7',
 '8',
 'asd'
  ];


//select lista loop

for(let i=0; i < itemList.length; i++)
{
  let option = document.createElement("option"),
  txt = document.createTextNode(itemList[i]);
  option.appendChild(txt);
  option.setAttribute("value",itemList[i]);
  selectList.insertBefore(option,selectList.lastChild);
}

//index szám loop

for(let i=0; i < itemList.length; i++)
{
  let option = document.createElement("option"),
  txt = document.createTextNode(itemList[i]);
  option.appendChild(txt);
  option.setAttribute("value",itemList[i]);
  indexNumber.insertBefore(option,indexNumber.lastChild);
}



  // feliratkozás

buttonAdd.addEventListener('click', OnButtonAddClick);
inputItemName.addEventListener('keyup', OnInputKeyup);

// táblázat listázás

RenderList();


function RenderList(){
  
    listItemList.innerText = '';
    itemList.forEach(function(item, index){
        RenderListItem(item);
    });
}

function RenderListItem(text){
    let newListItem = document.createElement('li');
    newListItem.innerText = text;
    listItemList.appendChild(newListItem);
}



function OnButtonAddClick() {
  // hozzáadás
  AddNewListItem();
}

function OnInputKeyup(event) {
  if (event.key == 'Enter') {
    // hozzáadás
    AddNewListItem();
  }
}

// egyéb függvények

function AddNewListItem() {
  // validálás
  if 
    (!inputItemName.value )
   {
    return;
  }

  // hozzáadás
  itemList.push(inputItemName.value);
  console.log(itemList);

  RenderList();
}
